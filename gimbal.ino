#include <MPU6050_6Axis_MotionApps20.h>
#include <I2Cdev.h>
#include <Servo.h>

#define PITCH 9
#define ROLL 10
#define YAW 11

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include <Wire.h>
#endif

#define INTERRUPT_PIN 2


MPU6050 mpu;
Servo pitchServo, rollServo, yawServo;

float ykp=1, pkp=1, rkp=1;
float yki=0, pki=0, rki=0;
float yawSetpoint=0, pitchSetpoint=0, rollSetpoint=0;
float yawError=0, pitchError=0, rollError=0; 
float yawOutput, pitchOutput, rollOutput;

VectorFloat gravity;
float rotationVals[3];
uint8_t mpuReady=0, mpuIntStatus, mpuStatus, packetSize=0, fifoCount=0, fifoBuffer[64];
volatile uint8_t mpuInterrupt = 0;
unsigned long timestep=200, currTime=0, debug_delay=2000;
Quaternion q;

void servo_init();
void mpu_init();
void control_init();
void dmp_data_ready();

//Control Functions
float compute_p_error(float, float, float);
float compute_i_error(float, float, float);
float compute_output(float, float);

void compute_yaw_error();
void compute_pitch_error();
void compute_roll_error();

void setup()
{
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    Wire.begin();
    Wire.setClock(400000);
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
    Fastwire::setup(400, true);
    #endif
	
	Serial.begin(9600);
	Serial.println("Initialising...");
	
	mpu_init();
	control_init();
	servo_init();
}

void loop()
{
	if(!mpuReady){
		Serial.println("CANNOT CONNECT TO MPU!. \n RECONNECT AND TRY AGAIN...");
			return;
	}
	
	while(!mpuInterrupt && (fifoCount < packetSize)){;}
		if((millis() - currTime) >= timestep){
			compute_yaw_error();
			compute_pitch_error();
			compute_roll_error();
			currTime = millis();
			/*break;*/
		}

	if(pitchOutput > 180 || rollOutput > 180 || yawOutput > 180){
		if(pitchOutput > 180){
			pitchOutput = 180;
		}
		if(rollOutput > 180){
			rollOutput = 180;
		}
		if(yawOutput > 180){
			yawOutput = 180;
		}
	}
	else if(pitchOutput <= 0 || rollOutput <= 0 || yawOutput <= 0){
		if(pitchOutput <= 0){
			pitchOutput = 0;
		}
		if(rollOutput <= 0){
			rollOutput = 0;
		}
		if(yawOutput <= 0){
			yawOutput = 0;
		}
	}
	//}
	
	pitchServo.write(180 - pitchOutput);
	rollServo.write(rollOutput);
	yawServo.write(yawOutput);
	
	mpuInterrupt = 0;
	mpuIntStatus = mpu.getIntStatus();
	
	fifoCount = mpu.getFIFOCount();
	if((mpuIntStatus & 0x10) || (fifoCount > 1023)){
		mpu.resetFIFO();
		
	}
	else if(mpuIntStatus & 0x02){
		while (fifoCount < packetSize){
			fifoCount = mpu.getFIFOCount();
		}
		mpu.getFIFOBytes(fifoBuffer, packetSize);
		fifoCount -= packetSize;
		
		mpu.dmpGetQuaternion(&q, fifoBuffer);
		mpu.dmpGetGravity(&gravity, &q);
		mpu.dmpGetYawPitchRoll(rotationVals, &q, &gravity);
		
		for(uint8_t i=0; i<3; ++i){
			rotationVals[i] *= (180/M_PI);
		}
		
		if ((millis() - debug_delay) > 2000){
			Serial.println("YAW\tPITCH\tROLL");
			Serial.print(rotationVals[0]);
			Serial.print("\t");
			Serial.print(rotationVals[1]);
			Serial.print("\t");
			Serial.print(rotationVals[2]);
			Serial.println("\n");
			debug_delay = millis();
		}
	}
}

//Initialization Functions

void servo_init()
{
	pitchServo.attach(PITCH);
	rollServo.attach(ROLL);
	yawServo.attach(YAW);
}

void mpu_init()
{
	/// Add complete list of initialization procedures..
	mpu.initialize();
	
	while(!mpu.testConnection()){
		Serial.println("CONNECTING TO MPU");
	}
	
	mpuStatus = mpu.dmpInitialize();
	
	mpu.setXGyroOffset(99);
	mpu.setYGyroOffset(13);
	mpu.setZGyroOffset(6);
	mpu.setZAccelOffset(1523);
	
	if(mpuStatus == 0){
		Serial.println("MPU CONNECT SUCCESSFUL");
		mpu.setDMPEnabled(true);
		attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN),dmp_data_ready, RISING);
		mpuIntStatus = mpu.getIntStatus();
		
		mpu.setSleepEnabled(false);
		mpuReady = 1;
		
		packetSize = mpu.dmpGetFIFOPacketSize();
	}
	else{
		Serial.println("CONNECTION ERROR:");
		Serial.println(mpuStatus);
		mpuReady = 0;
	}
}

void control_init()
{
	yawSetpoint = 0;
	pitchSetpoint = 0;
	rollSetpoint = 0;
	
	ykp = 50;
	pkp = 50;
	rkp = 50;
	
	yki = 0.5;
	pki = 0.5;
	rki = 0.5;
}

float compute_p_error(float input, float setpoint, float kp)
{
	float pError = setpoint - input;
	return (kp * pError);
}

float compute_i_error(float _error, float pError, float ki)
{
	float iError = _error + pError;
	return ((ki * iError * timestep)/1000);
}

float compute_output(float iError, float pError)
{
	return iError + pError;	 
}

void compute_yaw_error()
{
	float pError = compute_p_error((rotationVals[0] + 90), yawSetpoint, ykp);
	float iError = compute_i_error(yawError, pError, yki);
	yawError = iError;
	yawOutput = compute_output(iError, pError);
}

void compute_pitch_error()
{
	float pError = compute_p_error((rotationVals[1] + 90), pitchSetpoint, pkp);
	float iError = compute_i_error(pitchError, pError, pki);
	pitchError = iError;
	pitchOutput = compute_output(iError, pError);
}

void compute_roll_error()
{
	float pError = compute_p_error((rotationVals[2] + 90), rollSetpoint, rkp);
	float iError = compute_i_error(rollError, pError, rki);
	rollError = iError;
	rollOutput = compute_output(iError, pError);
}


void dmp_data_ready()
{
	mpuInterrupt = 1;
}